import React from "react";
import ReactDOM from "react-dom";
import AppNavbar from "./components/AppNavbar";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";

//Page Components
import Courses from "./pages/Courses";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import App from "./App";

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById("root")
);
