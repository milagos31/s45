import { useState, useEffect, useContext } from "react";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/esm/Container";
import Button from "react-bootstrap/esm/Button";
// import UserContext from "../UserContext";
import { UserContext } from "../App";

function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isDisabled, setIsDisabled] = useState(true);

    const { user, setUser } = useContext(UserContext);

    useEffect(() => {
        const conditions = [email !== "", password !== ""];
        conditions.every((element) => element) ? setIsDisabled(false) : setIsDisabled(true);
    }, [email, password]);

    const login = (e) => {
        e.preventDefault();

        setUser({ email: email });

        alert("you're now logged in as " + user.email);

        setEmail("");
        setPassword("");
    };
    return (
        <Container fluid>
            <h2>Login Page</h2>
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Email"
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value);
                        }}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        required
                        onChange={(e) => {
                            setPassword(e.target.value);
                        }}
                    />
                </Form.Group>
                <Button type="submit" variant="success" disabled={isDisabled}>
                    Login
                </Button>
            </Form>
        </Container>
    );
}
export default Login;
