import Container from "react-bootstrap/Container";
import Course from "../components/Course";

//Data imports
import courses from "../mock-data/courses";

function Courses() {
    const CourseCards = courses.map((course) => {
        return <Course course={course} key={course.id} />;
    });

    return <Container fluid>{CourseCards}</Container>;
}

export default Courses;
