import { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/esm/Container";
import Button from "react-bootstrap/esm/Button";

function Register() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isDisabled, setIsDisabled] = useState(true);

    //  useEffect(() => {
    //      console.log("email:", email);
    //  }, [email]);

    //  useEffect(() => {
    //      console.log("password:", password);
    //  }, [password]);

    //  useEffect(() => {
    //      console.log("confirmPassword:", confirmPassword);
    //  }, [confirmPassword]);

    useEffect(() => {
        const conditions = [email != "", password != "", confirmPassword == password];
        const result = conditions.every((element) => element);
        result ? setIsDisabled(false) : setIsDisabled(true);
    }, [email, password, confirmPassword]);

    const register = (e) => {
        e.preventDefault();
        alert("submitted");
    };

    return (
        <Container fluid>
            <h1>Registration Page</h1>
            <Form onSubmit={register}>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Input email here"
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value);
                        }}
                    />
                    <Form.Text className="text-muted">We'll never share your email</Form.Text>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Input password"
                        value={password}
                        required
                        onChange={(e) => {
                            setPassword(e.target.value);
                        }}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Confirm password"
                        value={confirmPassword}
                        required
                        onChange={(e) => {
                            setConfirmPassword(e.target.value);
                        }}
                    />
                </Form.Group>
                <Button type="submit" variant="primary" disabled={isDisabled}>
                    Submit
                </Button>
            </Form>
        </Container>
    );
}
export default Register;
