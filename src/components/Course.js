import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { useEffect, useState } from "react";

const Course = ({ course }) => {
    // let course = props.course;
    const [seats, setSeats] = useState(10);
    const [isDisabled, setIsDisabled] = useState(false);

    useEffect(() => {
        if (seats == 0) {
            setIsDisabled(true);
        }
    }, [seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>
                    <h4>{course.name}</h4>
                    <h5>Description:</h5>
                </Card.Title>
                <Card.Text>{course.description}</Card.Text>
                <h6>Price:</h6>
                <p>{course.price}</p>
                <h6>Enrollees (Seats available: {seats})</h6>
                <Button
                    variant="primary"
                    href="#enroll"
                    onClick={() => {
                        setSeats(seats - 1);
                    }}
                    disabled={isDisabled}
                >
                    Enroll
                </Button>
            </Card.Body>
        </Card>
    );
};

export default Course;
