//Bootstrap Component
import React from "react";
import { Link, NavLink } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

function AppNavbar() {
    return (
        //   <Navbar bg="light" expand="lg">
        //       <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
        //       <Navbar.Toggle aria-controls="basic-navbar-nav" />
        //       <Navbar.Collapse id="basic-navbar-nav">
        //           <Nav className="mr-auto">
        //               <Nav.Link href="#home">Home</Nav.Link>
        //               <Nav.Link href="/courses">Courses</Nav.Link>
        //           </Nav>
        //       </Navbar.Collapse>
        //   </Navbar>
        <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/">
                Zuitt Booking
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link as={NavLink} to="/">
                        Home
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">
                        Courses
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/register">
                        Register
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/login">
                        Login
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default AppNavbar;
