import { NavLink } from "react-router-dom";
import Container from "react-bootstrap/Container";

function NotFound() {
    return (
        <Container className="text-center">
            <h3>Page Not Found</h3>
            <p>
                Go back to the{" "}
                <NavLink as={NavLink} to="/">
                    Home Page
                </NavLink>
            </p>
        </Container>
    );
}
export default NotFound;
